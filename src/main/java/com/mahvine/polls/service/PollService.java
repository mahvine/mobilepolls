package com.mahvine.polls.service;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.bind.RelaxedPropertyResolver;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.mahvine.polls.model.Message;
import com.mahvine.polls.model.Poll;
import com.mahvine.polls.model.PollChoice;
import com.mahvine.polls.model.Vote;
import com.mahvine.polls.model.Voter;
import com.mahvine.polls.repository.MessageRepository;
import com.mahvine.polls.repository.PollChoiceRepository;
import com.mahvine.polls.repository.PollRepository;
import com.mahvine.polls.repository.VoteRepository;
import com.mahvine.polls.repository.VoterRepository;

@Service
public class PollService  implements EnvironmentAware{
	
	@Autowired
	MessageRepository messageRepository;
	
	@Autowired
	VoteRepository voteRepository;
	
	@Autowired
	VoterRepository voterRepository;
	
	@Autowired
	PollRepository pollRepository;
	
	@Autowired
	PollChoiceRepository pollChoiceRepository;
	

	private RelaxedPropertyResolver propertyResolver;
	
	private static final Logger logger = LoggerFactory.getLogger(PollService.class);
	
	public void castVote(Long pollId, Long choiceId, String deviceId){
		Voter voter = voterRepository.findByDeviceId(deviceId);
		if(voter == null){
			voter = new Voter();
			voter.deviceId = deviceId;
			voter = voterRepository.save(voter);
		}
		Poll poll = pollRepository.findOne(pollId);
		PollChoice pollChoice = pollChoiceRepository.findOne(choiceId);
		if(poll!=null){
			if(poll.choices.contains(pollChoice)){
				Vote vote = new Vote();
				vote.choice = pollChoice;
				vote.voter = voter;
				vote.poll = poll;
				vote.datetime = new Date();
				vote = voteRepository.save(vote);
				logger.info("cast a vote for poll:{} choice:{}",poll,pollChoice);
				
				//compute votes
				for(PollChoice choice: poll.choices){
					Long count = voteRepository.countByChoiceId(choice.id);
					choice.votes = count;
					pollChoiceRepository.save(choice);
				}
				
			}else{
				throw new IllegalArgumentException("Invalid choiceId");
			}
		}else{
			throw new IllegalArgumentException("Invalid pollId");
		}
	}
	
	

	public Long postMessage(Long pollId, String deviceId, String body, String tag){
		Voter voter = voterRepository.findByDeviceId(deviceId);
		if(voter == null){
			voter = new Voter();
			voter.deviceId = deviceId;
			voter = voterRepository.save(voter);
		}
		Poll poll = pollRepository.findOne(pollId);
		if(poll!=null){
			Vote vote = voteRepository.findByPollIdAndVoterId(pollId, voter.id);
			if(vote!=null){
				Message message = new Message();
				message.body = body;
				message.date = new Date();
				message.vote = vote;
				message.tag = tag;
				message = messageRepository.save(message);
				logger.info("message created:{}", message);
				return message.id;
			}else{
				throw new IllegalArgumentException("Cast a vote first");
			}
		}else{
			throw new IllegalArgumentException("Invalid pollId");
		}
	}
	


    @Scheduled(cron="${app.process.keep.alive}")
    public void keepAliveToSelf(){
    	RestTemplate restTemplate = new RestTemplate();
    	String endpoint = propertyResolver.getProperty("address", String.class, "http://mobilepolls-monetization.rhcloud.com/api-docs/index.html");
    	String indexPage = restTemplate.getForObject(endpoint, String.class);
    	logger.info("successful invoke of self:{}",indexPage.substring(0, 50));
    }


    @Override
    public void setEnvironment(Environment environment) {
        this.propertyResolver = new RelaxedPropertyResolver(environment, "service.");
    }
    

	
}
