package com.mahvine.polls.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="messages")
public class Message {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long id;
	
	public String tag;
	
	@ManyToOne
	public Vote vote;
	
	public String body;
	
	public Date date;

	@Override
	public String toString() {
		return "{" + (id != null ? "id:" + id + ", " : "")
				+ (tag != null ? "tag:" + tag + ", " : "")
				+ (vote != null ? "vote:" + vote.poll.id+"|"+vote.voter.id+"|"+vote.choice.id + ", " : "")
				+ (body != null ? "body:" + body + ", " : "")
				+ (date != null ? "date:" + date : "") + "}";
	}

	
	
}
