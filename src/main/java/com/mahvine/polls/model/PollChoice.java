package com.mahvine.polls.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="poll_choices")
public class PollChoice {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long id;
	
	public String name;
	
	public long votes;

	@Override
	public String toString() {
		return "{" + (id != null ? "id:" + id + ", " : "")
				+ (name != null ? "name:" + name + ", " : "") + "votes:"
				+ votes + "}";
	}
	
	

}
