package com.mahvine.polls.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name="polls")
public class Poll {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long id;
	
	@NotBlank
	public String name;
	
	@NotBlank
	public String description;
	
	@OneToMany
	public List<PollChoice> choices;

	@Override
	public String toString() {
		return "{"
				+ (id != null ? "id:" + id + ", " : "")
				+ (name != null ? "name:" + name + ", " : "")
				+ (description != null ? "description:" + description + ", "
						: "") + (choices != null ? "choices:" + choices : "")
				+ "}";
	}
	
	

}
