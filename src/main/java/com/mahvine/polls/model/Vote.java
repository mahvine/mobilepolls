package com.mahvine.polls.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.mahvine.polls.model.Vote.VotePK;

@Entity
@Table(name="votes")
@IdClass(VotePK.class)
public class Vote {

	@Id
	@ManyToOne
	@JoinColumn(name="voter_id")
	public Voter voter;
	
	@Id
	@ManyToOne
	@JoinColumn(name="poll_id")
	public Poll poll;
	

	@ManyToOne
	@JoinColumn(name="choice_id")
	public PollChoice choice;
	
	public Date datetime;
	
	
	public static class VotePK implements Serializable{
		private Long voter;
		
		@Override
		public String toString() {
			return "VotePK [" + (voter != null ? "voter=" + voter + ", " : "")
					+ (poll != null ? "poll=" + poll : "") + "]";
		}

		public Long getVoter() {
			return voter;
		}

		public void setVoter(Long voter) {
			this.voter = voter;
		}

		private Long poll;
		

		public Long getPoll() {
			return poll;
		}

		public void setPoll(Long poll) {
			this.poll = poll;
		}
		
	}
}
