package com.mahvine.polls.web.rest;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mahvine.polls.model.Message;
import com.mahvine.polls.model.Poll;
import com.mahvine.polls.model.PollChoice;
import com.mahvine.polls.model.Vote;
import com.mahvine.polls.repository.MessageRepository;
import com.mahvine.polls.repository.PollChoiceRepository;
import com.mahvine.polls.repository.PollRepository;
import com.mahvine.polls.repository.VoteRepository;
import com.mahvine.polls.service.PollService;
import com.mahvine.polls.web.dto.CastVoteRequest;
import com.mahvine.polls.web.dto.GetVoteDTO;
import com.mahvine.polls.web.dto.MessageDTO;
import com.mahvine.polls.web.dto.PostMessageRequest;
import com.mahvine.polls.web.rest.filter.MessageFilter;
import com.mahvine.polls.web.rest.util.PaginationUtil;

@RestController
@RequestMapping("/api")
public class PollResource {
	
	@Autowired
	PollService pollService;
	
	@Autowired
	PollRepository pollRepository;
	
	@Autowired
	PollChoiceRepository pollChoiceRepository;
	
	@Autowired
	MessageRepository messageRepository;
	
	@Autowired
	VoteRepository voteRepository;
	
	@RequestMapping(value="/polls", method=RequestMethod.POST)
	public void savePoll(@RequestBody Poll poll){
		List<PollChoice> choices = poll.choices;
		choices = pollChoiceRepository.save(choices);
		poll.choices = choices;
		pollRepository.save(poll);
	}

	@RequestMapping(value="/polls", method=RequestMethod.GET)
	public ResponseEntity<List<Poll>> listPolls(@RequestParam(value="size", required=false, defaultValue="10") int size,
			@RequestParam(value="page", required=false, defaultValue="1") int page) throws URISyntaxException{
		
		PageRequest pageRequest = PaginationUtil.generatePageRequest(page, size);
		Page<Poll> pagePoll = pollRepository.findAll(pageRequest);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(pagePoll, "/api/polls", page, size);
		return new ResponseEntity<List<Poll>>(pagePoll.getContent(),headers,HttpStatus.OK);	
	}

	@RequestMapping(value="/polls/{pollId}", method=RequestMethod.GET)
	public Poll getPollDetails(@PathVariable("pollId") Long pollId){
		return pollRepository.findOne(pollId);
	}
	

	@RequestMapping(value="/polls/{pollId}/votes", method=RequestMethod.POST)
	public void castVote(@PathVariable("pollId") Long pollId, @RequestBody CastVoteRequest request){
		pollService.castVote(pollId, request.choiceId, request.deviceId);
	}

	@RequestMapping(value="/polls/{pollId}/myvote", method=RequestMethod.GET)
	public ResponseEntity<GetVoteDTO> getVote(@PathVariable("pollId") Long pollId,@RequestParam("deviceId") String deviceId){
		Vote vote = voteRepository.findByPollIdAndVoterDeviceId(pollId, deviceId);
		
		GetVoteDTO getVoteDTO = null;
		HttpStatus status = HttpStatus.NOT_FOUND;
		if(vote!=null){
			getVoteDTO = new GetVoteDTO();
			getVoteDTO.choice = vote.choice;
			getVoteDTO.datetime = vote.datetime;
			status = HttpStatus.OK;
		}
		return new ResponseEntity<GetVoteDTO>(getVoteDTO, status);
	}
	
	@RequestMapping(value="/polls/{pollId}/messages", method=RequestMethod.POST)
	public ResponseEntity<List<MessageDTO>>  postMessages(@PathVariable("pollId") Long pollId, @RequestBody PostMessageRequest request,
			HttpServletRequest servletRequest){
		Long newMessageId = pollService.postMessage(pollId, request.deviceId, request.body, request.tag);
		return listMessages(pollId, request.idAfter,null, request.tag, 100, "id", Direction.DESC, servletRequest);
	}
	
	@RequestMapping(value="/polls/{pollId}/messages", method=RequestMethod.GET)
	public ResponseEntity<List<MessageDTO>> listMessages(@PathVariable("pollId") Long pollId, 
			@RequestParam(value="idAfter", required=false) Long messagesGreaterThanId,
			@RequestParam(value="idBefore", required=false) Long messagesLessThanId,
			@RequestParam(value="tag", required=false) String tag,
			@RequestParam(value="items", required=false, defaultValue="10") int items,
			@RequestParam(value="sort_by", required=false, defaultValue="id") String sortBy,
			@RequestParam(value="direction", required=false, defaultValue="DESC") Direction direction,
			HttpServletRequest servletRequest){
		Sort sort = new Sort(direction,sortBy);
		PageRequest pageRequest = PaginationUtil.generatePageRequest(1, items, sort);
		Page<Message> pageMessage = messageRepository.findAll(new MessageFilter(servletRequest,pollId,tag,messagesGreaterThanId),  pageRequest);
		HttpHeaders headers = new HttpHeaders();
		headers.set("X-total-count", pageMessage.getTotalElements()+"");
		
		List<MessageDTO> contents = new ArrayList<MessageDTO>();
		for(Message message: pageMessage){
			contents.add(new MessageDTO(message));
		}
		return new ResponseEntity<List<MessageDTO>>(contents, headers, HttpStatus.OK);
	}
	
	

}
