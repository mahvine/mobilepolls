/**
 *
 */
package com.mahvine.polls.web.rest.filter;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;

import com.mahvine.polls.model.Message;
import com.mahvine.polls.model.Poll;
import com.mahvine.polls.model.Vote;

/**
 * @author Jrdomingo
 * Jan 11, 2016 11:09:49 AM
 */
public class MessageFilter extends SpecificationFilter<Message>{

	private Long pollId;
	private String tag;
	private Long idAfter;
	
	
	/**
	 * @param request
	 */
	public MessageFilter(HttpServletRequest request, Long pollId, String tag, Long idAfter) {
		super(request);
		this.pollId = pollId;
		this.tag = tag;
		this.idAfter = idAfter;
	}

	/* (non-Javadoc)
	 * @see ph.com.smart.racetoace.web.rest.filter.SpecificationFilter#createPredicates(javax.persistence.criteria.Root, javax.persistence.criteria.CriteriaQuery, javax.persistence.criteria.CriteriaBuilder, java.util.List)
	 */
	@Override
	public void createPredicates(Root<Message> root, CriteriaQuery<?> criteria, CriteriaBuilder criteriaBuilder, List<Predicate> predicates) {

		Predicate pollFilter = criteriaBuilder.equal(root.<Vote>get("vote").<Poll>get("poll").<Long>get("id"), pollId);
		predicates.add(pollFilter);
		
		if(idAfter!=null){
			Predicate idAfterFilter = criteriaBuilder.greaterThan(root.<Long>get("id"), idAfter);
			predicates.add(idAfterFilter);
		}

		Long idBefore = getLongParam("idBefore");
		if(idBefore!=null){
			Predicate idLessFilter = criteriaBuilder.lessThan(root.<Long>get("id"), idBefore);
			predicates.add(idLessFilter);
		}
		
		

		if(tag!=null){
			Predicate tagFilter = criteriaBuilder.lessThan(root.<String>get("tag"), tag);
			predicates.add(tagFilter);
		}
	}


}
