package com.mahvine.polls.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mahvine.polls.model.Voter;
import com.mahvine.polls.repository.VoterRepository;
import com.mahvine.polls.web.dto.UpdateVoter;

@RestController
@RequestMapping("/api")
public class VoterResource {
	
	@Autowired
	VoterRepository voterRepository;

	@RequestMapping(value="voters/{deviceId}", method=RequestMethod.PUT)
	public void updateName(@PathVariable("deviceId") String deviceId, @RequestBody UpdateVoter request){
		Voter voter = voterRepository.findByDeviceId(deviceId);
		if(voter==null){
			voter = new Voter();
			voter.deviceId = deviceId;
		}
		voter.name = request.name;
		voterRepository.save(voter);
	}


	@RequestMapping(value="voters/{deviceId}", method=RequestMethod.GET)
	public ResponseEntity<Voter> getVoter(@PathVariable("deviceId") String deviceId){
		Voter voter = voterRepository.findByDeviceId(deviceId);
		
		HttpStatus status = HttpStatus.OK;
		if(voter == null){
			status = HttpStatus.NOT_FOUND;
		}
		return new ResponseEntity<Voter>(voter,status);
	}

	
}
