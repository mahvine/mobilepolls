package com.mahvine.polls.web.dto;

public class PostMessageRequest {
	
	public String body;
	
	public String deviceId;
	
	public Long idAfter;
	
	public String tag;
}
