/**
 *
 */
package com.mahvine.polls.web.dto;

import java.util.Date;

import com.mahvine.polls.model.PollChoice;

/**
 * @author JRDomingo
 * Apr 19, 2016 6:18:50 PM
 * TODO
 */
public class GetVoteDTO {

	public Date datetime;
	
	public PollChoice choice;
}
