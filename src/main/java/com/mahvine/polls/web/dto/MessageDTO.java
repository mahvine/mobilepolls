package com.mahvine.polls.web.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.mahvine.polls.model.Message;
import com.mahvine.polls.model.PollChoice;

@JsonInclude(value=Include.NON_NULL)
public class MessageDTO {

	public Long id;
	
	public String body;
	
	public PollChoice voterChoice;
	
	public String from;

	public Date date;
	
	public Long fromId;
	
	public MessageDTO(){
		
	}
	
	public MessageDTO(Message message){
		this.id = message.id;
		this.date = message.date;
		this.body = message.body;
		this.voterChoice = message.vote.choice;
		if(message.vote.voter.name == null){
			this.fromId = message.vote.voter.id;
		}else{
			this.from = message.vote.voter.name;
		}
	}
	
}
