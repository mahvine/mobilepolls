package com.mahvine.polls.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.mahvine.polls.model.Message;

@Repository
public interface MessageRepository extends JpaRepository<Message, Long>, JpaSpecificationExecutor<Message>{

	Page<Message> findByVotePollIdAndIdGreaterThan(Long pollId, Long messageId, Pageable pageable);

	Page<Message> findByVotePollIdAndIdGreaterThanAndTag(Long pollId, Long messageId, String tag, Pageable pageable);
	
	List<Message> findByIdGreaterThanAndIdLessThan(Long lastMessageId, Long messageId, Sort sort);
	
}
