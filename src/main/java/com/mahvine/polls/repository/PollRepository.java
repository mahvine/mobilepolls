package com.mahvine.polls.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mahvine.polls.model.Poll;

@Repository
public interface PollRepository extends JpaRepository<Poll,Long>{
	
	

}
