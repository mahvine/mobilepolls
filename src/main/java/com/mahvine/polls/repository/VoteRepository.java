package com.mahvine.polls.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mahvine.polls.model.Vote;
import com.mahvine.polls.model.Vote.VotePK;

@Repository
public interface VoteRepository extends JpaRepository<Vote,VotePK>{

	public Vote findByPollIdAndVoterId(Long pollId,Long voterId);

	public Vote findByPollIdAndVoterDeviceId(Long pollId,String deviceId);
	
	public Long countByChoiceId(Long choiceId);
	
}
