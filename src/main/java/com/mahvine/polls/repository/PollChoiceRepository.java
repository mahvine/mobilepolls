package com.mahvine.polls.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mahvine.polls.model.PollChoice;

@Repository
public interface PollChoiceRepository extends JpaRepository<PollChoice, Long>{
	

}
