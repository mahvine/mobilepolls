package com.mahvine.polls.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mahvine.polls.model.Voter;

@Repository
public interface VoterRepository extends JpaRepository<Voter,Long>{
	
	public Voter findByDeviceId(String deviceId);

}
