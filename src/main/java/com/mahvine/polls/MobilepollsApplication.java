package com.mahvine.polls;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MobilepollsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MobilepollsApplication.class, args);
	}
}
